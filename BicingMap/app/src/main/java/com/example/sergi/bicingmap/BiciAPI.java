package com.example.sergi.bicingmap;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sergi on 30/01/18.
 */

public class BiciAPI {
    private final String BASE_URL = "http://wservice.viabicing.cat/v2";

    ArrayList<Bici> getBicis() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("stations")
                .build();
        String url = builtUri.toString();

        try {
            String JsonResponse = HttpUtils.get(url);
            return proccesJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    private ArrayList<Bici> proccesJson(String jsonResponse) {
        ArrayList<Bici> bicis = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonBicis = data.getJSONArray("stations");
            for (int i = 0; i<jsonBicis.length();i++){
                JSONObject jsonBici = jsonBicis.getJSONObject(i);

                Bici bici = new Bici();
                bici.setId(jsonBici.getInt("id"));
                bici.setTipo(jsonBici.getString("type"));
                bici.setLatitud(jsonBici.getString("latitude"));
                bici.setLongitud(jsonBici.getString("longitude"));
                bici.setBicis(jsonBici.getInt("bikes"));
                bici.setSitios(jsonBici.getInt("slots"));
                bici.setTipo(jsonBici.getString("type"));
                bicis.add(bici);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bicis;

    }
}
