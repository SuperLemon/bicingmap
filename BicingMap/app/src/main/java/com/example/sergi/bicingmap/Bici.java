package com.example.sergi.bicingmap;

import java.io.Serializable;

/**
 * Created by sergi on 23/01/18.
 */

public class Bici implements Serializable {
    int id;
    String tipo;
    String latitud;
    String longitud;
    int sitios;
    int bicis;
    Estado estado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public int getSitios() {
        return sitios;
    }

    public void setSitios(int sitios) {
        this.sitios = sitios;
    }

    public int getBicis() {
        return bicis;
    }

    public void setBicis(int bicis) {
        this.bicis = bicis;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
